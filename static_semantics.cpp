//static_semantics.cpp
//This is the file for running the static semantics
//Justin Henn
//Assignment 3
//4/22/18

#include <stdlib.h>
#include <iostream>
#include <string>
#include <string.h>
#include <vector>
#include <sstream>
#include "stack_node.h"
#include "stack.h"
#include "node.h"

using namespace std;

FILE* outfile;
static int temp_variable_counter = 0;
static int in_label_counter = 0;
static int out_label_counter = 0;
int read_int = 0;

vector<int> counter;

void static_semantics(node_t* root, int varCount);

stack_node* front;

/*function for starting the semantics check */

void start_semantics(node_t* root, string output_name) {


  outfile = fopen(output_name.c_str(), "w");
  
  init(front);

  int z = 0;
  counter.push_back(0);

  static_semantics(root, z);
    for (int x = 0; x < counter[counter.size()-1]; x++) {

      front = pop(front);
      fprintf(outfile, "POP\n");
    }

  fprintf(outfile, "STOP\n");

  fprintf(outfile, "R 0\n");

  for (int x = 0; x < temp_variable_counter; x++)

    fprintf(outfile, "T%d 0\n", x);

  fclose(outfile);

}


string make_out_label () {

  string str = "Out";
  stringstream ss;
  ss << out_label_counter;
  str += ss.str();
  out_label_counter++;
  return str;
}

string make_in_label () {

  string str = "In";
  stringstream ss;
  ss << in_label_counter;
  str += ss.str();
  in_label_counter++;
  return str;
}

string make_temp_variable() {

  string str = "T";
  stringstream ss;
  ss << temp_variable_counter;
  str += ss.str();
  temp_variable_counter++;
  return str;
}

/*function that does that static semantics checking*/

void static_semantics(node_t* root, int varCount) {

  varCount = counter.size()-1;
  node_t* temp = root;
  if(temp == NULL) {

    return;
  }

  if (temp->key == "block"){
    counter.push_back(0);

 static_semantics(temp->left, varCount);
  static_semantics(temp->mid_left, varCount);
  static_semantics(temp->mid_right, varCount);
  static_semantics(temp->right, varCount);

    varCount = counter.size();
    for (int x = 0; x < counter[varCount-1]; x++) {

      front = pop(front);
      fprintf(outfile, "POP\n");
    }

    counter.pop_back();
  return;
  }

  int stack_check = 0;
  if (!(temp->different_tokens).empty()) {

    if (temp->key == "vars" || temp->key == "mvars") {

      if (counter[varCount] > 0) {

        stack_check = find(front, (temp->different_tokens)[0].instance);
        if (stack_check < counter[varCount] && stack_check > -1) {

          cout << "Error in " << temp->key << " " << (temp->different_tokens)[0].instance << "\n" ;
          exit (1);
        }
      }
      if (temp->key == "vars")
        fprintf(outfile,"LOAD %s\n", (temp->different_tokens)[1].instance.c_str());

      front = push(front, (temp->different_tokens)[0].instance);
      fprintf(outfile,"PUSH\n");
      fprintf(outfile, "STACKW 0\n");
      counter[varCount] = counter[varCount] + 1; 
    }
    else if (!isalpha((temp->different_tokens)[0].instance[0])) {

    }

    else {

      stack_check = find(front, (temp->different_tokens)[0].instance);
      if (stack_check == -1) {

        cout << "Error in " << temp->key << " " << (temp->different_tokens)[0].instance << "\n";
        exit (1);
      }
    }
  }

  if (temp->key == "expr" && !(temp->different_tokens).empty()) {

    static_semantics(temp->mid_left, varCount); 
    string temporary = make_temp_variable(); 
    fprintf(outfile, "STORE %s\n", temporary.c_str()); 
    static_semantics(temp->left, varCount);
    if ((temp->different_tokens)[0].token_type == "+Tk") { 

      fprintf(outfile,"ADD %s\n", temporary.c_str()); 
    }
    else if ((temp->different_tokens)[0].token_type == "-Tk") { 

      fprintf(outfile,"SUB %s\n", temporary.c_str()); 
    }
    else if ((temp->different_tokens)[0].token_type == "*Tk") { 

      fprintf(outfile,"MULT %s\n", temporary.c_str()); 
    }
    else
      fprintf(outfile,"DIV %s\n", temporary.c_str()); 

    return;
  }

  if (temp->key == "out") {

    static_semantics(temp->left, varCount);
    string temporary = make_temp_variable(); 
    fprintf(outfile, "STORE %s\n", temporary.c_str()); 
    fprintf(outfile, "WRITE %s\n", temporary.c_str()); 
    return;
  }
  if (temp->key == "R" && !(temp->different_tokens).empty()) {

    if ((temp->different_tokens)[0].token_type == "IDTk") {

      fprintf(outfile,"STACKR %d\n", stack_check);
    }
    else
      fprintf(outfile, "LOAD %s\n", (temp->different_tokens)[0].instance.c_str());
  }
  if (temp->key == "in") {

    fprintf(outfile, "READ R\n");
    fprintf(outfile, "LOAD R\n");
    fprintf(outfile, "STACKW %d\n", stack_check);

  }
  if (temp->key == "assign") {

    static_semantics(temp->left, varCount);
    fprintf(outfile, "STORE R\n");
    fprintf(outfile, "LOAD R\n");
    fprintf(outfile, "STACKW %d\n", stack_check);
    return;
  }
  if (temp->key == "M") {

    if(!(temp->different_tokens).empty()) {

      static_semantics(temp->left, varCount);
      fprintf(outfile, "MULT -1");
      return;
    }
  }


  if(temp->key == "If") {

    static_semantics(temp->mid_right,varCount);
    string temporary = make_temp_variable(); 
    fprintf(outfile, "STORE %s\n", temporary.c_str()); 
    static_semantics(temp->left, varCount);
    fprintf(outfile, "SUB %s\n", temporary.c_str()); 
    string label = make_out_label();

    if ((temp->mid_left->different_tokens).size() > 1) {

      if ((temp->mid_left->different_tokens)[1].token_type == ">Tk") {

        fprintf(outfile, "BRNEG %s\n", label.c_str());

      }
      else if ((temp->mid_left->different_tokens)[1].token_type == "<Tk") {

        fprintf(outfile, "BRPOS %s\n", label.c_str());
      }
      else {

        fprintf(outfile, "BRPOS %s\n", label.c_str());
        fprintf(outfile, "BRNEG %s\n", label.c_str());
      }
    }
    else {

      if ((temp->mid_left->different_tokens)[0].token_type == ">Tk") {

        fprintf(outfile, "BRZNEG %s\n", label.c_str());

      }
      if ((temp->mid_left->different_tokens)[0].token_type == "<Tk") {

        fprintf(outfile, "BRZPOS %s\n", label.c_str());

      }
      if ((temp->mid_left->different_tokens)[0].token_type == "=Tk") {

        fprintf(outfile, "BRZERO %s\n", label.c_str());

      }
    }
    static_semantics(temp->right, varCount);
    fprintf(outfile,"%s: NOOP\n", label.c_str());
    return;
  }
  if(temp->key == "loop") {
    string out_label = make_out_label();
    string in_label = make_in_label();
    fprintf(outfile, "%s: NOOP\n", in_label.c_str());

    static_semantics(temp->mid_right,varCount);
    string temporary = make_temp_variable(); 
    string temporary1 = make_temp_variable(); 



    fprintf(outfile, "STORE %s\n", temporary.c_str()); 
    static_semantics(temp->left, varCount);
    fprintf(outfile, "STORE %s\n", temporary1.c_str()); 
    fprintf(outfile, "LOAD %s\n",temporary1.c_str()); 
    fprintf(outfile, "SUB %s\n", temporary.c_str()); 

    if ((temp->mid_left->different_tokens).size() > 1) {

      if ((temp->mid_left->different_tokens)[1].token_type == ">Tk") {

        fprintf(outfile, "BRNEG %s\n", out_label.c_str());

      }
      else if ((temp->mid_left->different_tokens)[1].token_type == "<Tk") {

        fprintf(outfile, "BRPOS %s\n", out_label.c_str());
      }
      else {

        fprintf(outfile, "BRPOS %s\n", out_label.c_str());
        fprintf(outfile, "BRNEG %s\n", out_label.c_str());
      }
    }
    else {

      if ((temp->mid_left->different_tokens)[0].token_type == ">Tk") {

        fprintf(outfile, "BRZNEG %s\n", out_label.c_str());

      }
      else if ((temp->mid_left->different_tokens)[0].token_type == "<Tk") {

        fprintf(outfile, "BRZPOS %s\n", out_label.c_str());

      }
      else  {

        fprintf(outfile, "BRZERO %s\n", out_label.c_str());

      }
    }
    static_semantics(temp->right, varCount);

    fprintf(outfile, "BR %s\n", in_label.c_str());


    fprintf(outfile,"%s: NOOP\n", out_label.c_str());
    return;
  }

  static_semantics(temp->left, varCount);
  static_semantics(temp->mid_left, varCount);
  static_semantics(temp->mid_right, varCount);
  static_semantics(temp->right, varCount);
  /*if(temp->key == "block") {

    for (int x = 0; x <= varCount; x++) {

      front = pop(front);
      fprintf(outfile, "POP\n");
    }
  }*/
}
