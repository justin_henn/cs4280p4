CC	= g++		# The C compiler
CFLAGS	= -g		# Enable debugging by default
TARGET	= comp
OBJS	= main.o parser.o scanner.o tree.o static_semantics.o stack.o

$(TARGET): $(OBJS)
	$(CC) -o $(TARGET) $(OBJS)

main.o:	main.cpp
	$(CC) $(CFLAGS) -c main.cpp

parser.o: parser.cpp
	$(CC) $(CFLAGS) -c parser.cpp

scanner.o: scanner.cpp
	$(CC) $(CFLAGS) -c scanner.cpp

static_semantics.o: static_semantics.cpp
	$(CC) $(CFLAGS) -c static_semantics.cpp

stack.o: stack.cpp
	$(CC) $(CFLAGS) -c stack.cpp

tree.o: tree.cpp
	$(CC) $(CFLAGS) -c tree.cpp
clean:
	/bin/rm -f *.o *~ *.sp18 *.txt *.out *.asm $(TARGET)
