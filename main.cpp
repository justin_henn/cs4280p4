//main
//This is the main file for the parser
//Justin Henn
//Assignment 2
//4/15/18
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <string.h>
#include "parser.h"
#include "static_semantics.h"

using namespace std;

int main(int argc, char **argv) {

/*Variables needed from main*/

  FILE*  myfile;
  char arg1[1024];
  const char ADD_ON[] = ".sp18";
  string filename = "";
  string output_name = "";
  
/*Check if file argument*/

  if (argc > 1) {

    strcpy(arg1, argv[1]);
    strcat(arg1, ADD_ON);

    if ((myfile = fopen(arg1, "r")) == NULL) {

      printf("Cannot open file.\n");
      return -1;
    }
  }

  /*If no file argument*/

  else {

    ofstream ofile;
    ofile.open("input.txt");
    string s;

    if (!ofile.is_open()) {
 
      cout << "Unable to open file\n";
      return 0;
  }
    while(getline(cin, s)) {

      ofile << s << "\n";

    }
    ofile.close();
    if ((myfile = fopen("input.txt", "r")) == NULL) {

      printf("Cannot open file.\n");
      return -1;
    }
  }

 /*check if file is not empty*/

  fseek(myfile, 0, SEEK_END);
  int size = ftell(myfile);

  if (size == 0) {
    printf("file is empty\n");		
  }
  rewind(myfile);
  fclose(myfile);
  
  /*get output file names*/

  if (argc > 1) {

    filename = arg1;
    output_name = filename + ".asm";

  }
  else {

    filename = "input.txt";
    output_name = "out.asm";
  }

  /*do semantics operations*/

  node_t* root =  parser(root, filename);
  start_semantics(root, output_name);
  cout << "OK\n";
}
