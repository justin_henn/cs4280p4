//stack.cpp
//This is the file for creating and using the stack
//Justin Henn
//Assignment 3
//4/22/18

#include <stdio.h>
#include <string>
#include "stack.h"
#include "stack_node.h"
#include <stdlib.h> 
#include <iostream>

using namespace std;

/*initialize stack*/

void init(stack_node* head) {

  head =  NULL;
}

/*push onto stack*/

stack_node* push(stack_node* head, string data) {

  stack_node* temp = new stack_node;
  temp->variable = data;
  temp->next = head;
  head = temp;
  return head;
}

/*pop from stack*/

stack_node* pop(stack_node* head) {

  head = head->next;
  return head;
}

/*find something on stack*/

int find(stack_node* head, string data) {

  stack_node* current = head;
  int count = 0;

  while (current != NULL) {

    if (current->variable == data) {
     
      return count; 
    }
    count++;
    current = current->next;
  }
  return -1; 
}
