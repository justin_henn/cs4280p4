//stack_node.h
//This is the stack node h file
//Justin Henn
//Assignment 3
//4/22/18
#ifndef STACK_NODE_H
#define STACK_NODE_H
#include <stdio.h>
#include <string>

using namespace std;

typedef struct stack_node {

  string variable;
  stack_node* next;
}stack_node;
#endif
