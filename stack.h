//stack.h
////This is the stack h file
//Justin Henn
//Assignment 3
//4/22/18

#ifndef STACK_H
#define STACK_H
#include <string>
#include "stack_node.h"

using namespace std;

stack_node* push(stack_node* head, string data);
stack_node* pop(stack_node* head);
int find(stack_node* head, string data);
void init(stack_node* head);
#endif
