//scanner.h
//This is the h file for the scanner
//Justin Henn
//Assignment 2
//4/2/18
#ifndef SCANNER_H
#define SCANNER_H
#include "token.h"

token get_token(FILE *filePtr, char nextChar, int line_number);

#endif
